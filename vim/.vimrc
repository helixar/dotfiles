call plug#begin('~/.vim/plugged')

Plug 'VundleVim/Vundle.vim'
Plug 'chr4/nginx.vim'
Plug 'itchyny/lightline.vim'
Plug 'airblade/vim-gitgutter'
Plug 'rstacruz/vim-closer'
Plug 'mattn/emmet-vim'
Plug 'arcticicestudio/nord-vim'

call plug#end()

try
    colorscheme nord
catch /^Vim\%((\a\+)\)\=:E185/
    " deal with it
endtry

syntax on
set tabstop=4
set expandtab
set softtabstop=4
set shiftwidth=4
set modelines=1
set autoindent
set smartindent
set smarttab
set number
set nocursorline
set wildmenu
set lazyredraw
set showmatch
set ruler

set mouse=a

inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
